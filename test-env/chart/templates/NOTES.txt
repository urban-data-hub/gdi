+--------------------+
|gdi test environment|
+--------------------+

# QGIS
http://qgis.gdi.test/
http://qgis.gdi.test/?SERVICE=WMS&REQUEST=GetCapabilities&MAP=testfile

# Masterportal
http://masterportal.gdi.test/

# Git
NOT VISIBLE IN WEB INTERFACE BEFORE LOGIN!
username: gdi
password: gdipass

http://git.gdi.test/gdi/postgis
http://git.gdi.test/gdi/qgis
http://git.gdi.test/gdi/masterportal

git clone http://gdi:gdipass@git.gdi.test/gdi/postgis
git clone http://gdi:gdipass@git.gdi.test/gdi/qgis
git clone http://gdi:gdipass@git.gdi.test/gdi/masterportal

API UI
http://git.gdi.test/api/swagger
