import unittest
import requests

QGIS_CAPABILITIES_URL = ("http://qgis.gdi.test/"
                         "?SERVICE=WMS&REQUEST=GetCapabilities")


class TestEndToEnd(unittest.TestCase):

    def test_qgis_project_title_from_git_synced_file(self):
        self.assertIn("<Title>testtitle</Title>",
                      requests.get(
                          QGIS_CAPABILITIES_URL + "&MAP=testfile").text)

    def test_qgis_project_file_missing(self):
        self.assertFalse(requests.get(QGIS_CAPABILITIES_URL).ok)

    def test_qgis_project_file_empty(self):
        self.assertFalse(requests.get(QGIS_CAPABILITIES_URL + "&MAP=").ok)

    def test_qgis_project_file_key_case(self):
        self.assertTrue(
            requests.get(QGIS_CAPABILITIES_URL + "&mAp=testfile").ok)

    def test_qgis_project_file_illegal_character(self):
        self.assertFalse(
            requests.get(QGIS_CAPABILITIES_URL + "&MAP=/testfile").ok)

    def test_masterportal_git_file(self):
        self.assertTrue(requests.get(
            "http://masterportal.gdi.test/additional-file.txt").ok)
