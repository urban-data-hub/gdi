# Test Environment

The files within this directory can be used to quickly start a test environment for gdi.
The environment serves as a testbed for the Helm chart (proper environment variables, manifests make sense),
and the integration parts between the components.

The test environment is only meant for local development and isolated test environments,
it is intentionally kept simple with hardcoded values (including test passwords) to simplify use and further development.

Do note that the integrated Git server is not persistent.
Once the pod restarts all data is gone.
This is a good thing because if you accidentally broke something you can fix it by deleting the pod and waiting 1-2 minutes for the periodic (re)configuration jobs to set things up again,
but it also means that any test repository you had is lost.
It is therefore strongly recommended that you keep a local copy (outside of the test environment, using the `git clone` commands printed at the end of the start script) if you are developing a real configuration to be pushed elsewhere.

## Prerequisites

Only a basic Linux setup with a few GNU utilities such as `sed` and `grep` is needed.

As long as the system allows for proper operation of a `kind` cluster, usually inside a single Docker container, most compatibility aspects should not cause any problems as almost everything apart from the setup script is contained within the `kind` cluster in that one Docker container.

## Starting / Updating

Run `test-env/start.sh` whenever you want to start or update the test environment.

This will (re)start a local `kind` Kubernetes cluster, apply changes to images, restart all affected pods, and update the deployed Kubernetes resources.

## E2E Tests

This environment also serves as the basis for the end-to-end test in the CI pipeline.

There are multiple variants of the e2e pipeline jobs because of different ways GitLab pipeline runners can be configured concerning Docker daemon access.
This should enable test runs on most prepared self-hosted GitLab installations and on gitlab.com.

Run the e2e tests locally with

```
python3 -m unittest discover test-env -v
```

## Debugging

Use all the usual debugging tools around Python / Kubernetes.

While the Flux-deployed Helm charts that are part of the development setup should retry deployment automatically,
the ones deployed as part of the main chart do not, see this [comment](https://github.com/fluxcd/helm-controller/issues/454#issuecomment-1607830077).
You can get a list of Helm releases managed by flux with `kubectl get helmreleases`.
Use `kubectl describe helmreleases YOUR_RELEASE_NAME` to get more details
and `for i in suspend resume; do flux $i -n default helmrelease YOUR_RELEASE_NAME; done` to retry.
