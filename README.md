# GDI

This project contains the image sources and Helm chart
needed to create a Kubernetes deployment for QGIS and Masterportal.

It is designed to be used within a larger deployment
that supplies the connected services and configures required secrets.

## Configuration

Both QGIS and Masterportal can be configured through Git repositories that are integrated through [git-sync](https://github.com/kubernetes/git-sync).

In most Git services you can configure read-only credentials for repositories that you can use for deployments.

## Local Development

A local development setup is available, see [test-env/README.md](./test-env/README.md).

## Contributing

### Code Formatting

To keep code formatting consistent, multiple programs are run as part of the CI pipeline.

Most files are checked with [Prettier](https://prettier.io/).
Since the pipeline currently uses ad-hoc images from nixery.dev, the used version is the version they provide.
You can find out the current version using `docker run nixery.dev/nodepackages.prettier prettier --version`.
To re-format files simply run `npx prettier@2.8.8 --write FILES...` (replace version).

### Release Script

Call `./release.sh` with the desired version number and follow the instructions. Trigger the Helm upload pipeline job manually in GitLab after pushing the tag.

The target Helm repository can be used as described in the [GitLab documentation](https://docs.gitlab.com/ee/user/packages/helm_repository/), for example `https://gitlab.com/api/v4/projects/53059999/packages/helm/main`.

### Commit requirements

All commits require a [DCO](https://developercertificate.org/) sign-off, which can be generated automatically by [calling `git commit` with `--signoff`](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---signoff).

Please include a body in the commit message if you think it helps others understand what you did and why.

Basic commit requirements are enforced by the pipeline, for details see [.conform.yaml](./.conform.yaml).
