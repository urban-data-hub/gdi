#!/bin/bash

set -eu -o pipefail

exec "$(dirname "$0")/common/release.sh" "${1-}" \
	charts/gdi/{Chart,values}.yaml \
	qgis/server/validmap/metadata.txt
