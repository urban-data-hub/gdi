# Common Files

Various similar projects have similar structures and requirements regarding the development and test environment.
The files within this directory are meant to be used by all of those projects to avoid senseless copy+paste
and to take advantage of improvements made in one project in all the others.

## Assumptions

- The project has a single Helm chart in `/charts/CHART_NAME/`.
- The project has a `test-env` subdirectory that contains a `chart` directory for the additional infrastructure to deploy into a `kind` cluster for testing and development.
- All `Dockerfile`s found indicate an image to be built and used in the Helm chart.
- The path to but not including the `Dockerfile` converted to `camelCase` and with elements separated by `.` is the location of the [Bitnami Common ImageRoot](https://artifacthub.io/packages/helm/bitnami/common#imageroot) used to configure the image.
- CI is done using GitLab.
- The default branch is `main`.
- The version on the default branch is not the current or the next version but a placeholder version of `0.0.1`, images are configured as `latest`.

Depending on what common files you use not all of those assumptions are relevant.

## Provided Functionality

- Building of container images using Kaniko
- Helm chart rendering with default values to spot fatal templating problems quickly
- Code style enforcemennt for various languages
- Commit policy enforcement (commit message, DCO)
- Local test environment based on `kind` that can be started with a single command
- End-to-end test environment on GitLab (self-hosted and gitlab.com)
- Very simple release script
- Helm packaging and upload to GitLab package registry
- Very simple local reference checking for links in Markdown files (links have to start with `./`)
- [Simple Helm diff script](./helm-templated-diff.sh) that can be used to confirm (absence of) changes during template refactoring.

## Collaboration Between Projects

These files are included directly in the repository to make using them as convenient as possible.
For synchronization between projects it uses [subtree](https://git.kernel.org/pub/scm/git/git.git/plain/contrib/subtree/git-subtree.txt) scripts.
You can think of the contents of this directory as being their own repository within the outer repository,
you can see the history of this `common` repository in the Git commit graph.

While it is possible to maintain `common` in a dedicated repository,
it is probably easiest to improve it while working on the actual projects and synchronize the projects from time to time.
To do this, simply call [common/pull-from-repo.sh](./pull-from-repo.sh) with the path to the other source repository.
