#!/bin/bash

set -eu -o pipefail

cd "$(dirname "$0")"

tmpdir="`mktemp -d`"
ref=main

if ! [[ "${VERSION=${1-}}" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]
then
	echo "This script must be called with a version number, like"
	echo "$0 1.2.3"
	exit 1
fi
shift

if [[ -n "`git tag --list "$VERSION"`" ]]
then
	echo "The Git tag $VERSION already exists!"
	exit 1
fi

git worktree add --detach "$tmpdir" "$ref"
cd "$tmpdir"

sed "s/0\.0\.1\|latest/$VERSION/" -i "$@"

git commit --all --message "Release $VERSION"
git tag "$VERSION"

cd -
rm -r "$tmpdir"
git worktree remove "$tmpdir"

echo "Tag $VERSION was created locally from $ref, please verify and push if satisfied."
