#!/bin/bash

set -eu -o pipefail

if [[ ! -v 1 ]]
then
	echo "Usage: $0 other-repository-with-common [ref]"
	echo "Call this script from the root directory of the target repository."
	exit 1
fi

if [[ ! -e .git ]]
then
	echo ".git not found!"
	exit 1
fi

PREFIX=common

git fetch -- "$@"

mkdir -- "$PREFIX" &>/dev/null || true
other_common=`git subtree split -P "$PREFIX" FETCH_HEAD`
rmdir -- "$PREFIX" &>/dev/null || true

sync () {
	git subtree $1 -m "${1^} $PREFIX" -P "$PREFIX" . $other_common
}

sync pull || sync add
