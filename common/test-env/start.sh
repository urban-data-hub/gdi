#!/bin/bash

# This script sets up a local test environment.

cd "$(dirname "$0")/.."

. test-env/common.sh

if [[ ${1-no} != yes ]]
then
	cat <<-.
	THIS SCRIPT WILL MODIFY YOUR SYSTEM IN MANY WAYS, NAMELY
	- download and install the kind binary if it is not found, see https://kind.sigs.k8s.io/
	- download and install kubectl if not found
	- download and install the Flux CLI if not found, see https://fluxcd.io/
	- download and install Helm if not found
	- create a local cluster using kind, this is typically contained within a Docker container, see kind docs for details
	- add a suitable entry to your /etc/hosts to allow you to access the test environment
	sudo is used as needed without additional confirmations.
	Review the script if you want, then call this script with an argument of 'yes'.
	.
	exit 1
fi

echo "Setting up local development environment..."

root_or_sudo () {
	if [[ $EUID -eq 0 ]]
	then
		"$@"
	else
		sudo "$@"
	fi
}

if ! kind version &>/dev/null
then
	tmp=`mktemp --suffix -kind`
	echo "Downloading and installing kind"

	curl -Lf# -o "$tmp" https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-amd64
	root_or_sudo install "$tmp" /usr/local/bin/kind

	rm "$tmp"
fi

if ! kubectl --help &>/dev/null
then
	tmp=`mktemp --suffix -kubectl`
	echo "Downloading and installing kubectl"

	curl -Lf# -o "$tmp" "https://dl.k8s.io/release/$KUBERNETES_VERSION/bin/linux/amd64/kubectl"
	root_or_sudo install "$tmp" /usr/local/bin/kubectl

	rm "$tmp"
fi

flux --help &>/dev/null || curl -s https://fluxcd.io/install.sh | root_or_sudo bash

helm version &>/dev/null || curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

kind get clusters | grep "^$KIND_CLUSTER$" || kind_create
[[ -e "$KUBECONFIG" ]] || kind get kubeconfig --name "$KIND_CLUSTER" > "$KUBECONFIG"

. test-env/build-deploy.sh

IP=`kubectl get node --output=jsonpath={.items[0].status.addresses[0].address}`
HOSTS_LINE="$IP `kubectl get ingress -A -o=jsonpath='{.items[*].spec.rules[*].host}'`"
grep -Fx "$HOSTS_LINE" /etc/hosts >/dev/null \
|| root_or_sudo sed -i.backup -e"\$a$HOSTS_LINE" -e"/^$IP /d" /etc/hosts

helm get notes test-env

if [[ -v KUBECONFIG ]]
then
	cat <<-.
	To use the local Kubernetes cluster, run
	export KUBECONFIG=$KUBECONFIG
	.
fi
