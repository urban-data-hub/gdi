#!/bin/bash

# This file contains common bash code and default values used in multiple scripts and pipeline snippets.

set -eu -o pipefail

: "${KUBERNETES_VERSION=v1.27.3}"

if [[ ! -e /var/run/secrets/kubernetes.io/serviceaccount/token ]]
then
	: "${KUBECONFIG=$PWD/test-env/kubeconfig}"
	export KUBECONFIG
fi

# determine a suitable short name based on the directory name under charts/
shortname () {
	local name=`basename charts/*`
	if <<<$name grep - &>/dev/null
	then
		<<<$name tr - \\n | cut -c1 | tr -d \\n
	else
		echo $name
	fi
}

: "${LOCALIMAGE_REGISTRY=localimage.test}"
: "${LOCALIMAGE_REPO_PREFIX=}"

: "${KIND_CLUSTER=kind}"

kind_create () {
	kind create cluster --name "$KIND_CLUSTER" \
		--image="kindest/node:$KUBERNETES_VERSION" \
		--config=test-env/kind-config.yaml

	${KUBECTL-kubectl} apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.4/manifests/calico.yaml
}

kind_delete () {
	kind delete cluster --name "$KIND_CLUSTER"
}
