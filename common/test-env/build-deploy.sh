#!/bin/bash

# This script is used by both start.sh and the CI pipeline to start both the test environment and the components
# using either the images specified through the LOCALIMAGE_ environment variables or local images built by this script
# you probably don't want to use this directly but instead call start.sh.

. test-env/common.sh

flux version || flux install

kubectl apply -f test-env/default-cluster-admin.yaml

# make test domain available inside cluster
kubectl -n kube-system get configmap coredns -oyaml \
| sed '/^ *rewrite/d;s/^\( *\)kubernetes/\1rewrite name regex ^[^.]+\\.[^.]+\\.test\\.$ ingress-nginx-controller.ingress-nginx.svc.cluster.local answer auto\n\0/g' \
| kubectl apply -f -
kubectl rollout restart -n kube-system deployment coredns
kubectl rollout status  -n kube-system deployment coredns &

valuePath () {
	# kebab to camel case with support for at least the abbreviation ...DB, `/` becomes `.`
	sed 's/-\([a-zA-Z]\([a-zA-Z]\($\|[^a-zA-Z]\)\)\?\)/\U\1/g;s/\//./g' <<<$1
}

image_opts=""
for dockerfile in `find -name Dockerfile`
do
	context=`dirname $dockerfile`
	context=${context#./}
	repo=$LOCALIMAGE_REPO_PREFIX$context
	image=$LOCALIMAGE_REGISTRY/$repo

	if [[ -v LOCALIMAGE_TAG ]]
	then
		tag=$LOCALIMAGE_TAG
	else
		tar chC $context --exclude-vcs{,-ignores} . | docker build -t $image -
		tag=`docker image inspect -f{{.Id}} $image | cut -f2 -d:`
		docker tag $image $image:$tag
		kind load docker-image --name "$KIND_CLUSTER" $image:$tag &
	fi
	image_opts="$image_opts --set `valuePath $context`.image.repository=$repo"
	image_opts="$image_opts --set `valuePath $context`.image.tag=$tag"
	image_opts="$image_opts --set `valuePath $context`.image.registry=$LOCALIMAGE_REGISTRY"
done

echo "Waiting for images to be loaded into kind node and/or CoreDNS to restart..."
wait

while :
do
	echo "|" # GitLab does not show empty lines https://gitlab.com/gitlab-org/gitlab/-/issues/217231
	kubectl get pod -A
	sleep 5
done &
CLEANUP_COMMAND="kill $! &>/dev/null && sleep 0.5"
trap "$CLEANUP_COMMAND" EXIT

all_in () {
	kubectl get $1 -o=jsonpath='{.items[?(@.metadata.annotations.meta\.helm\.sh/release-name=="'$2'")].metadata.name}'
}

wait_for_helm () {
	if [[ -v 1 ]]
	then
		kubectl wait --for=condition=Ready --all helmrelease --timeout=10m "$@"
	else
		wait_for_helm $(all_in helmrelease test-env) $(all_in helmrelease `shortname`)
	fi
}

# get rid of Jobs - possibly from CronJobs - that might hog the few local resources and cause a deployment deadlock
kubectl delete job --all

helm upgrade --install test-env ./test-env/chart/ --set testEnv.imagePrefix="$LOCALIMAGE_REGISTRY/$LOCALIMAGE_REPO_PREFIX" $image_opts
wait_for_helm nginx
helm upgrade --dependency-update --wait --timeout 15m --install `shortname` ./charts/*/ --values ./test-env/values.yaml $image_opts
wait_for_helm

kubectl rollout status deployment $(all_in deployment `shortname`)

eval "$CLEANUP_COMMAND"
trap - EXIT
