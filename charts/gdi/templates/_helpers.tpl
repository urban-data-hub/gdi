{{- define "gdi.ingress.annotations" -}}
{{ coalesce .annotations .context.Values.global.ingress.annotations | toYaml }}
{{- end -}}

{{- define "gdi.secretValue" -}}
valueFrom:
  secretKeyRef:
    name: {{ tpl .secret.secret (merge (dict "kebab" (kebabcase .name)) .context) | quote }}
    key: {{ .secret.key | quote }}
{{- end -}}

{{- define "gdi.securityContext" -}}
securityContext:
  runAsNonRoot: true
  runAsUser: 1000
  runAsGroup: 1000
  fsGroup: 1000
  seccompProfile:
    type: RuntimeDefault
{{- end -}}

{{- define "gdi.standard.image" -}}
image: {{ include "common.images.image" (dict "imageRoot" .image "global" .context.Values.global) }}
{{- if .image.pullPolicy }}
imagePullPolicy: {{ .image.pullPolicy }}
{{- end }}
{{- end -}}

{{- define "gdi.standard.pdb" -}}
{{- if and .Values.global.pdb (dig .name "enabled" false .Values.AsMap) -}}
{{- $kebab := kebabcase .name -}}
---
apiVersion: policy/v1
kind: PodDisruptionBudget
metadata:
  name: {{ printf "%s-%s" (include "common.names.fullname" .) $kebab | quote }}
spec:
  minAvailable: 1
  selector:
    matchLabels: {{- include "common.labels.matchLabels" . | nindent 6 }}
      app.kubernetes.io/component: {{ $kebab }}
{{- end -}}
{{- end -}}

{{- define "gdi.standard.git-sync" }}
  - name: git-sync
    image: registry.k8s.io/git-sync/git-sync:v4.1.0
    args:
      - --root=/data/
      - --link=current
      {{- range $k, $v := .config }}
      - {{ print "--" $k "=" $v | quote }}
      {{- end }}
    volumeMounts:
      - name: git-sync-data
        mountPath: /data/
    {{- if .secret }}
      - name: git-secret
        mountPath: /secret/
    {{- end }}
    readinessProbe:
      exec:
        command:
          - ls
          - /data/current
volumes:
  - name: git-sync-data
    emptyDir: {}
  {{- if .secret }}
  - name: git-secret
    secret:
      secretName: {{ .secret }}
  {{- end }}
{{- end -}}

{{- define "gdi.networkPolicy.publicEgress" -}}
# deny cluster-internal traffic except DNS by default
egress:
  - to:
      - ipBlock:
          cidr: 0.0.0.0/0
          except: [10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16]
  - to:
      - podSelector:
          matchLabels:
            k8s-app: kube-dns
        namespaceSelector:
          matchLabels:
            kubernetes.io/metadata.name: kube-system
    ports:
      - protocol: UDP
        port: 53
      - protocol: TCP
        port: 53
{{- end -}}
